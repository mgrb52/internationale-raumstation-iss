# Hausübung #2: "Mashup-App" - Internationale Raumstation ISS
Dies ist die Prüfungsleistung für das Modul Webbasierte Systeme. (SoSe 2020) 

Autor: Marco Grabiec, Matrikelnummer: 5273214  
Webanwendung: https://raumstation.herokuapp.com/


### Beschreibung der Mashup-APP

Die erstellte Webanwendung mit dem Titel „Internationale Raumstation ISS“ beinhaltet interessante Fakten über die ISS und eine Karte mit der aktuellen Position von dieser. Die Position wird insgesamt alle drei Sekunden aktualisiert auf der Karte angezeigt.
Insgesamt werden drei REST APIs miteinander verwendet: Zum einen die „Mapbox“ API für die Erstellung der Karte und der Markierung der einzelnen Positionen. Zum anderen werden die „Where the ISS at?“ und die „Realtime Satellite Tracking Map“ APIs für die Abfragen der Längen- und Breitengrade verwendet.

  ![Screenshot](Abgabe/use case.PNG)
  
### Installation unter Node.js

Um die App zu installieren wird Node.js und der Paketmanager npm benötigt. Nach der Installation können die folgenden Schritte durchlaufen werden.  
•	Clone das GIT-Repository  
•	Öffne ein dein Terminal und begib dich in den Ordner  
•	Starte das Projekt mit dem Befehl  "node app.js"  
•	Die Webanwendung sollte nun unter http://127.0.0.1:3000 im Browser verfügbar sein  

### API Übersicht

„Mapbox“: Diese API wird verwendet, um eine interaktive Karte auf der Webseite einzubinden.   
https://docs.mapbox.com/api/  
„Where the ISS at?“: ist eine REST-API, welche den Standort der Internationalen Raumstation (ISS) in Echtzeit verfolgt. Weiterhin werden Informationen, Längen- Breitengrad, Geschwindigkeit und Höhe der ISS im JSONFormat zur Verfügung stellt.  
https://wheretheiss.at/w/developer  
„Realtime Satellite Tracking Map“: bietet weiter Standorten von Satelliten in Echtzeit an.  
https://www.n2yo.com/?s=33591  

### Farbauswahl

Für die Primär- und Sekundärfarbe habe ich mich für folgende Farbkombination entschieden.
Mir schien der Blauton angemessen bei des Themas meiner Seite, und so ergab sich mein Farbschema aus diesen beiden Farben. In meinen Augen ergänzen sich die beiden Farbtöne gut und keiner von beiden sticht zu sehr hinaus. Eine zu grelle bzw. helle Farbe hätte in meinen Augen zu viel Aufmerksamkeit auf sich gezogen und hätte mit dem primären Blauton nicht gut harmoniert.
So ist die Farbgestaltung angenehm, einfach aber dennoch nicht uninteressant.  
Primärfarbe: #1D809F  
Sekundärfarbe: #9a67ea  
  ![Screenshot](Abgabe/triadic.PNG)  
  ![Screenshot](Abgabe/farben.PNG)  
  
  Außerdem beinhaltet die Webanwendung ein Darktheme, welches durch einen Button an der unteren Seite gewählt werden kann. So haben sie die Möglichkeit bei dunklerem Licht die Seite auch dunkel zu machen, um ihre Augen ein wenig zu schonen.
  
  

