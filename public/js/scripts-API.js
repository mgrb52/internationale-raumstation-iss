//JS für die Verwendung der zwei APIs
//Erstellung der Karte mit der "MapBox" API
mapboxgl.accessToken = 'pk.eyJ1IjoiYmF1bXVocjIiLCJhIjoiY2tkeG83azFmMzRhZTJ3dHZoOWloam93dCJ9.0DZOLSxC9e6QpGTvWmr0tg';

//Karte erstellen
let map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/dark-v10', //stylesheet location
        center: [30, 0], // starting position [lng, lat]
        zoom: 2 // starting zoom
});

//Marker erstellen - ISS
const el = document.createElement('div');
el.className = 'marker';
const marker = new mapboxgl.Marker(el).setLngLat([0, 0]).setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML('<h3 style="color: black">ISS</h3><p style="color: black">Funtkionen:<br>Unterkunft, Forschung, Amateurfunk, Tracking- und Datenrelais-Satellitensystem</p>')).addTo(map);

//Marker erstellen - NOAA 19
const el2 = document.createElement('div2');
el2.className = 'marker2';
const marker2 = new mapboxgl.Marker(el2).setLngLat([0, 0]).setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML('<h3 style="color: black">NOAA-19</h3><p style="color: black">Funtkion:<br>Bilder von Wolken und Oberflächenmerkmalen & atmosphärischen Temperatur und Luftfeuchtigkeit zur Verwendung in numerischen Wetter </p>')).addTo(map);
//Marker erstellen - INTEGRAL
const el3 = document.createElement('div3');
el3.className = 'marker3';
const marker3 = new mapboxgl.Marker(el3).setLngLat([0, 0]).setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML('<h3 style="color: black">Integral</h3><p style="color: black">Funktion:<br> Weltraum- und Geowissenschaften</p>')).addTo(map);


//ISS Informationen mit der "Where the ISS at?" API
const iss_api = 'https://api.wheretheiss.at/v1/satellites/25544';
//NOAA-19 Informationen mit der "N2YO.com" API
const noaa19 = 'https://www.n2yo.com/rest/v1/satellite/positions/33591/41.702/-76.014/0/2/&apiKey=FXU72Q-XL7BLC-BFTJKB-4J8N?';
//INTEGRAL Informationen mit der "N2YO.com" API
const integral = 'https://www.n2yo.com/rest/v1/satellite/positions/27540/41.702/-76.014/0/2/&apiKey=FXU72Q-XL7BLC-BFTJKB-4J8N?';

let first_center = true; // Zentriert die Map nur beim erstem Mal

async function getISSInformation() {
        //ISS
        const res_information_iss = await fetch(iss_api); //Die Antwort von der API
        const all_information_iss = await res_information_iss.json(); //Jason Format
        const{latitude, longitude, velocity, altitude} = all_information_iss;
        console.log("ISS: " +latitude +", " +longitude);

        //Noaa19
        const res_information_noaa19 = await fetch(noaa19); //Die Antwort von der API
        const all_information_noaa19 = await res_information_noaa19.json(); //Jason Format
        console.log("Noaa19: " +all_information_noaa19.positions[0].satlatitude +", " +all_information_noaa19.positions[0].satlongitude);

        //INTEGRAL
        const res_information_INTEGRAL = await fetch(integral); //Die Antwort von der API
        const all_information_INTEGRAL = await res_information_INTEGRAL.json(); //Jason Format
        console.log("Integral: " +all_information_INTEGRAL.positions[0].satlatitude +", " +all_information_INTEGRAL.positions[0].satlongitude);


        const heute = new Date(); //aktuelles Datum und Zeit

        // Karte in der Mitte zentrieren
        if (first_center == true){
                map.flyTo({center: [longitude, latitude]});
                first_center = false;
        }

        //Marker an die richtige Position bringen
        marker.setLngLat([longitude, latitude]);
        marker2.setLngLat([all_information_noaa19.positions[0].satlongitude, all_information_noaa19.positions[0].satlatitude]);
        marker3.setLngLat([all_information_INTEGRAL.positions[0].satlongitude, all_information_INTEGRAL.positions[0].satlatitude]);

        //Ausgabe der ISS Informationen
        document.getElementById('lat').textContent = latitude.toFixed(2);
        document.getElementById('long').textContent = longitude.toFixed(2);
        document.getElementById('speed').textContent = velocity.toFixed(0);
        document.getElementById('altitude').textContent = altitude.toFixed(0);

        let min = ""; let sec = "";
        if(heute.getMinutes() <= 9) min = "0";
        if(heute.getSeconds() <= 9) sec = "0";
        document.getElementById('time').textContent = "" +heute.getHours() +":" +min +heute.getMinutes() +":" +sec +heute.getSeconds();
}
getISSInformation();
setInterval(getISSInformation,3000);//Die Informationen werden alle 3 Sekunden aktualisiert
